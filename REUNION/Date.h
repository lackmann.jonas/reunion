#pragma once
#include <string>
#include <ctime>

/*
	Klasse um Datum zu speichern
*/
class Date
{
public:
	Date(std::string Tag, std::string Monat, std::string Jahr);
	static Date getCurrentDate();
	std::string toString();
	~Date();
private:
	std::string Tag;
	std::string Monat;
	std::string Jahr;
};


