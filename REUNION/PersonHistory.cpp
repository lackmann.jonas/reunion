#include "PersonHistory.h"



PersonHistory::PersonHistory(Date aenderungsDatum, std::string Vorname, std::string Nachname,
	std::string Geburtsname, std::string Adresse, std::string email, std::string telNR, std::string kommentar)
	:aenderungsDatum(aenderungsDatum),Vorname(Vorname),Nachname(Nachname),Geburtsname(Geburtsname),Adresse(Adresse),
	email(email),telNR(telNR),kommentar(kommentar)
{
}

/*
	Liefert alten Datenstand mit Änderungsdatum als String zurück.
*/
std::string PersonHistory::toString()
{
	return aenderungsDatum.toString() + "\n" + Vorname + " " + Nachname + "\n" + Geburtsname +"\n" + Adresse +  "\n"+ telNR+"\n" + kommentar+"\n";
}


PersonHistory::~PersonHistory()
{
}
