#pragma once

#include <QWidget>
#include "ui_SeitenMenu.h"

class Organisator;
class LoginScreen;
/*
	Seitliches menu im Hautpfenster.
*/
class SeitenMenu : public QWidget
{
	Q_OBJECT

public:
	SeitenMenu(QWidget *parent = Q_NULLPTR);
	void setUser(Organisator* user);
	void setLoginScreen(LoginScreen *screen);
	~SeitenMenu();

private slots:

	void onPasswortAendern();
	void onLogout();
	void onPersonErstellen();

private:
	Organisator *user;
	LoginScreen *login_screen;
	Ui::SeitenMenu ui;
};
