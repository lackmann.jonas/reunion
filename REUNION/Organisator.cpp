#include "Organisator.h"
#include <string>
#include "UserType.h"
#include "PersonList.h"

Organisator::Organisator(std::string VName, std::string NName, std::string email, std::string passwort)
	:Person(VName,NName,email)
{
}

Organisator::Organisator(std::string VName, std::string NName, std::string email, std::string adr, std::string telNr, std::string komment,  std::string passwort)
	:Person(VName,NName,email,adr,telNr,komment)
{
}

Organisator::~Organisator()
{
}

/*
	Liefert ein Organisator-Objekt basierend auf einer Datenbank-Abfrage zur�ck

	Statische Methode
*/
Organisator* Organisator::login(std::string email, std::string passwort)
{

	/*
	Datenbank query einf�gen
	*/
	return new Organisator("", "",email, passwort); //objekt erstellen
}

/*
	Eigentlich �berfl�ssig, da SQLite keine Benutzer kennt, welche sich abmelden m�ssten.
*/
bool Organisator::logout()
{
	return false;
}


/*
	Aktualisiert die �bergbene Person mit den �bergbenen neuen Daten
*/
bool Organisator::datenBearbeiten(Person *p, std::string newNName, std::string gebName, std::string newAdr, std::string newTel, std::string newKomment)
{

	auto type = UserType::getUserType(p->getEMail()); 
	//Wenn �bergebene Person kein Organisator ist, oder der aktuelle User ein Hautporganisator ist. Dieser darf alle Daten auch der der Organisatoren bearbeiten
	if (type == UserType::Person || UserType::getUserType(this->getEMail()) == UserType::Hauptorganisator ) {

		/*
			Wenn die neuen Daten von den alten abweichen
		
		*/
		if(newNName != p->getNName())
			p->aendereNachname(newNName);
		if(newAdr != p->getAdresse())
			p->aendereAdresse(newAdr);

		if(newTel != p->getTelNR())
			p->aendereTel(newTel);

		if(newKomment != p->getKommentar())
			p->aendereKommentar(newKomment);

		if (p->getGebName() == "") {
			p->setGebName(gebName);
		}

		return true;
	}
	else {
		return false;
	}

	
}

/*
	F�gt eine neue Person dem System hinzu.
*/
bool Organisator::personHinzufuegen(Person *p)
{
	//Person wird einfach in die Zentrale Datenhaltung eingef�gt.
	PersonList::instance().insert(p);
	return true;
}

bool Organisator::passwortAendern(std::string neuesPasswort)
{
	return false;
}
