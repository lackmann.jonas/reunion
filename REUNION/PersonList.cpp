#include "PersonList.h"
#include "Person.h"
#include <algorithm>

PersonList::PersonList()
	:Subject()
{
}

PersonList::PersonList(const PersonList &)
	: Subject()
{
}


/*
	Liefert eine Referenz auf die Instanz 
*/
PersonList& PersonList::instance()
{
	static PersonList _instance;
	return _instance;
}


/*
	F�gt eine Person hinzu und informiert alle Observer
*/
void PersonList::insert(Person * p)
{
	this->list.push_back(p);
	notify();
}

/*
	Liefert eine Person anhand ihrer ID 
*/
Person * PersonList::get(int id)
{
	for (Person* x : list) {
		if (x->id == id) {
			return x;
		}
	}
	return nullptr;
}


/*
	L�scht die Person mit der �bergebenen ID
*/
bool PersonList::del(int id)
{
	//sucht nach dem Person Objekt
	auto it = std::find_if(list.begin(), list.end(), [=](Person* p) {
		return p->id == id;
	});

	//Falls es einen Treffer gab ist it != list.end() und das Objekt wird gel�scht.
	if (it != list.end()) {
		list.erase(it);
		notify();	//benachrichtigt alle Observer

		return true;
	}
	return false;
}


/*
	Leert die komplette Liste
*/
void PersonList::clear()
{
	list.clear();
	notify();

}


/*
	Liefert alle Person-Objekte
*/
std::vector<Person*>& PersonList::getAll()
{
	return list;
}

PersonList::~PersonList()
{
}

