#pragma once
#include <string>


/*
	Klasse um festzustellen welche art User es sich bei einer bestimmten e-mail handelt.
*/
class UserType
{
public:
	enum Type {
		Hauptorganisator,
		Organisator,
		Person
	};

	static Type getUserType(std::string email);


	
};

