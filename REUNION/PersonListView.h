#pragma once

#include <QListWidget>
#include "ui_PersonListView.h"
#include "Observer.h"
#include "PersonList.h"

class Organisator;
/*
	Widget in dem alle Personen aufgelistet werden.
*/
class PersonListView : public QListWidget, public Observer
{
	Q_OBJECT

public:
	PersonListView(QWidget *parent = Q_NULLPTR);
	virtual void update();
	void setUser(Organisator *user);
	~PersonListView();

private:
	Organisator* user;
	void addPerson(Person *p);
	Ui::PersonListView ui;
};
