#pragma once

#include <QWidget>
#include "ui_LoginScreen.h"
#include "REUNION.h"
#include <string>

class Organisator;

/*
	Klasse f�r den Login Screen.
*/
class LoginScreen : public QWidget
{
	Q_OBJECT

public:
	LoginScreen(QWidget *parent = Q_NULLPTR);
	~LoginScreen();

private slots:

	void onDatenbankWaehlen();
	void onLogin();
	void onPasswortvergessen();

private:
	REUNION w;
	Organisator *subject;
	std::string DBfilename;
	Ui::LoginScreen ui;
};
