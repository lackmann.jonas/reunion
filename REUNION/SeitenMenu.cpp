#include "SeitenMenu.h"
#include "PersonEditView.h"
#include "PersonEditViewHauptOrga.h"
#include "LoginScreen.h"
#include "Organisator.h"
#include "Hauptorganisator.h"
#include "UserType.h"

SeitenMenu::SeitenMenu(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);


	connect(ui.PersonErstellenPushButton, &QPushButton::clicked, this, &SeitenMenu::onPersonErstellen);

	connect(ui.PWAendernpushButton, &QPushButton::clicked, this, &SeitenMenu::onPasswortAendern);

	connect(ui.LogoutPushButton, &QPushButton::clicked, this, &SeitenMenu::onLogout);

}


void SeitenMenu::setUser(Organisator * user)
{
	this->user = user;
	ui.UserLabel->setText(QString::fromStdString(user->getVName()) + " " + QString::fromStdString(user->getNName()));
	
	/*
		check for user role
	*/
	if(UserType::getUserType(user->getEMail()) == UserType::Hauptorganisator){
		ui.RoleLabel->setText("Hauptorganisator");
	}
	else {
		ui.RoleLabel->setText("Oragnisator");
	}
	
}

void SeitenMenu::setLoginScreen(LoginScreen * screen)
{
	this->login_screen = screen;
}



void SeitenMenu::onLogout()
{

	//Zeigt Login Screen 
	login_screen->show();

	//Schlie�t das Hauptfenster
	QApplication::activeWindow()->close();
}

void SeitenMenu::onPersonErstellen()
{
	//Pr�ft welcher Benutzertyp angemeldet ist.
	if (UserType::getUserType(user->getEMail()) == UserType::Hauptorganisator) {
		PersonEditViewHauptOrga edit(nullptr, static_cast<Hauptorganisator*>(user));
		edit.exec();
	}
	else {
		PersonEditView edit(nullptr, user);
		edit.exec();
	}


}


void SeitenMenu::onPasswortAendern() {

	/*
		Routinen f�r Passwort �ndern einf�gen.
	*/

}

SeitenMenu::~SeitenMenu()
{
}