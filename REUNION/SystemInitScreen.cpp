#include "SystemInitScreen.h"
#include <qfiledialog.h>
#include "Hauptorganisator.h"
#include "LoginScreen.h"
#include "CreateDatabase.h"

SystemInitScreen::SystemInitScreen(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	connect(ui.CreateDBButton, &QPushButton::clicked, this, &SystemInitScreen::onDatenbankAnlegen);

	connect(ui.ErstellenPushButton, &QPushButton::clicked, this, &SystemInitScreen::onLogin);

}

SystemInitScreen::~SystemInitScreen()
{
	
}

/*
	L�sst Benutzer den Speicherort der Datenbank ausw�hlen
*/
void SystemInitScreen::onDatenbankAnlegen() {

	DBFileName = QFileDialog::getOpenFileName(this).toStdString();
	
}


void SystemInitScreen::onLogin()
{
	if (DBFileName != "" &&ui.VNameLineEdit->text() != "" && ui.NNameLineEdit->text() != "" && ui.EmailLineEdit->text() != "") {
	
		
		
		/*
		Datenbank und Hauptorganisator anlegen


		*/
		
		CreateDatabase::datenbankErstellen(DBFileName);

		Hauptorganisator H(ui.VNameLineEdit->text().toStdString(), ui.NNameLineEdit->text().toStdString(),
			ui.GeburtsnameLineEdit->text().toStdString(), ui.AdresseEdit->toPlainText().toStdString(), 
			ui.TelNRLineEdit->text().toStdString(), "", ui.PasswortEdit->text().toStdString());

		this->close();

		QApplication::quit();//Schlie�t die Anwendung
	}

}
