#pragma once
#include <string>
#include <vector>
#include "PersonHistory.h"
#include "QtPersonDAO.h"

/*
	Liefert eine Eindeutige ID zurück
*/
class genID {
public:
	static int generateID() {
		return ++id;
	}

	~genID() {}
private:
	genID() {}
	static genID instance;
	static int id;
};



/*
	Repräsentiert eine Person im System.
*/
class Person
{
public:
	Person(std::string VName, std::string NName, std::string email);
	Person(std::string VName, std::string NName, std::string email, std::string adr, std::string telNr, std::string komment);
	~Person();
private:
	std::string Vorname;
	std::string Geburtsname;
	std::string Nachname;
	std::string Adresse;
	std::string Tel_Nr;
	std::string e_mail;
	std::string kommentar;
	std::vector<PersonHistory *>history;
	PersonDAO *DAO;
public:
	void aendereNachname(std::string newName);
	void aendereAdresse(std::string neueAdr);
	void aendereTel(std::string neueTel);
	void aendereKommentar(std::string neuerKommentar);
	void setGebName(std::string gebName);
	std::vector<PersonHistory*>& getHistory();
	int id;

	std::string getVName();
	std::string getNName();
	std::string getGebName();
	std::string getAdresse();
	std::string getTelNR();
	std::string getEMail();
	std::string getKommentar();



};

