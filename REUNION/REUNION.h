#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_REUNION.h"
class LoginScreen;
class Organisator;
/*
	Hauptfenster
*/
class REUNION : public QMainWindow
{
	Q_OBJECT

public:
	REUNION(QWidget *parent = Q_NULLPTR);
	void setLoginScreen(LoginScreen *s);
	void setUser(Organisator* user);
private:
	Organisator *user;
	LoginScreen *login;
	Ui::REUNIONClass ui;
};
