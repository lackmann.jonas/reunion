#include "REUNION.h"
#include "LoginScreen.h"
#include <QtWidgets/QApplication>
#include <string>
#include "SystemInitScreen.h"
#include "PersonList.h"
#include "Person.h"
#include "Organisator.h"
#include "Hauptorganisator.h"


int main(int argc, char *argv[])
{
	
	
	

	/*
		Anwendung mit Testdaten bef�llen
	*/
	if (argc > 1 && std::string(argv[1]) == "test") {

		PersonList::instance().insert(new Hauptorganisator("Roland", "Dietrich", "rd@mail.de", "RD"));
		PersonList::instance().insert(new Organisator("Max", "Mustermann", "MM@mail.de", "MM"));
		PersonList::instance().insert(new Organisator("Klara", "Musterfrau", "KM@mail.de", "KM"));

		PersonList::instance().insert(new Person("Hans", "Guenter", "guenter@hans.de"));
		PersonList::instance().insert(new Person("Klaus", "Ernst", "ernst@mail.de", "Hochweg 19, 5434 Stadt", "",""));
		PersonList::instance().insert(new Person("Hans", "Schmied", "schmied@msn.de"));
		PersonList::instance().insert(new Person("Ulla", "Gans", "blumenfee@web.de"));
		PersonList::instance().insert(new Person("Max", "Schmidt", "max@web.de"));
		PersonList::instance().insert(new Person("Moritz", "M�ller", "M_M@mail.de"));
		PersonList::instance().insert(new Person("Felix", "Schneider", "FS@gmail.de"));
		PersonList::instance().insert(new Person("Lukas", "Meyer", "LM@mail.de"));
		PersonList::instance().insert(new Person("Sebastian", "Hofmann", "SH@web.de"));
		PersonList::instance().insert(new Person("Frida", "Wagner", "FW@gmail.de"));
	}


	/*
	Beim ersten Start wird die Anwendung initialisiert.
	Dazu wird der init-screen aufgerufen um Datenbank und Hauptorganisator anzuglegen
	Beim Schlie�en des Fensters wird die Anwendung neu gestartet und der Nutzer muss sich erneut anmelden
	*/
	if (argc > 1 && std::string(argv[1]) == "clear") {
		
		QApplication a(argc, argv);
		SystemInitScreen init;
		init.show();
		a.exec();
	}


	QApplication a(argc, argv);
	LoginScreen login; 
	login.show();
	return a.exec();
}
