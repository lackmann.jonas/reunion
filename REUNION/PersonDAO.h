#pragma once

#include <string>
#include <vector>
class Person; 
/*
	Abstraktes DataAccessObjekt der Person-Klasse
*/
class PersonDAO
{
public:
	
	virtual bool insert(Person *p) = 0;
	virtual bool update(Person *p) = 0;
	virtual bool remove(Person *p) = 0;
	virtual bool search(Person *p) = 0;
	virtual bool select(std::string name, std::vector<Person*>&ps) = 0;
	
};

