#pragma once
#include "Person.h"

/*
	Repräsentiert eine Organisator im System
*/
class Organisator :
	public Person
{
public:
	Organisator(std::string VName, std::string NName, std::string email, std::string passwort);
	Organisator(std::string VName, std::string NName, std::string email, std::string adr, std::string telNr, std::string komment, std::string passwort);
	~Organisator();
	static Organisator* login(std::string email, std::string passwort);
	bool logout();
	bool datenBearbeiten(Person *p, std::string newNName,std::string gebName, std::string newAdr, std::string newTel, std::string newKomment);
	bool personHinzufuegen(Person *p);
	bool passwortAendern(std::string neuesPasswort);

};

