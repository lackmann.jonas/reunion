#pragma once
#include "Organisator.h"

class string;
/*
	Hauptorganisator, der das System aufsetzt und verwaltet.
*/
class Hauptorganisator :
	public Organisator
{
public:

	Hauptorganisator(std::string VName, std::string NName, std::string email, std::string passwort);
	Hauptorganisator(std::string VName, std::string NName, std::string email, std::string adr, std::string telNr, std::string komment, std::string passwort);
	~Hauptorganisator();
	static Hauptorganisator* login(std::string email, std::string passwort);
	bool organisatorBenennen(Person *p);
	std::string passwortVergeben(Organisator *o);
};

