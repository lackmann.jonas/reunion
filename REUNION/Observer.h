#pragma once
/*
Abstrakte Oberklasse f�r Observer-Objekte des Subjekt/Observer-Pattern
*/
class Observer
{
public:
	Observer();
	virtual void update() = 0;
	~Observer();
};

