#include "LoginScreen.h"
#include <qmainwindow.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include "Organisator.h"
#include "Hauptorganisator.h"
#include "UserType.h"

LoginScreen::LoginScreen(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	connect(ui.LOGINpushButton, &QPushButton::clicked, this, &LoginScreen::onLogin);

	connect(ui.DBAuswaehlen, &QPushButton::clicked, this, &LoginScreen::onDatenbankWaehlen);

	connect(ui.PWVergessenPushButton, &QPushButton::clicked, this, &LoginScreen::onPasswortvergessen);


}

LoginScreen::~LoginScreen()
{
}

void LoginScreen::onLogin()
{



	if (DBfilename != "" && ui.EmailLineEdit->text() != "" && ui.PWlineEdit->text() != "") { // Datenbank, email und passwort m�sse vorhanden sein

		UserType::Type uT = UserType::getUserType(ui.EmailLineEdit->text().toStdString()); //Pr�fen welcher Benutzertyp

		if (uT == UserType::Hauptorganisator) {
			subject = Hauptorganisator::login(ui.EmailLineEdit->text().toStdString(), ui.PWlineEdit->text().toStdString());
		}
		else {
			subject = Organisator::login(ui.EmailLineEdit->text().toStdString(), ui.PWlineEdit->text().toStdString());
		}
		

		w.setLoginScreen(this);//Login Screen am Hauptfenster registrieren
		w.setUser(subject);//den angemeldeten Nutzer weitergeben
		w.show();

		this->close();

	}

}

void LoginScreen::onPasswortvergessen()
{

	QMessageBox dialog(this);
	dialog.setText("Hauptorganisator wurde benachrichtigt.\nAuf Rueckmeldung warten.");
	dialog.exec();


	/*
		Routinen f�r Benachrichtigung einf�gen
	*/
}

void LoginScreen::onDatenbankWaehlen() {
	DBfilename = QFileDialog::getOpenFileName(this).toStdString();

}
