#pragma once
#include <vector>
#include "Observer.h"

/*
	Oberklasse f�r die Subjects des Subjekt/Observer-Patterns
*/
class Subject
{
public:
	Subject();
	void attach(Observer* obs);
	void notify();
	~Subject();

private:
	std::vector<Observer*>observers;
};

