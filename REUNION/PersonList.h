#pragma once
#include <vector>
#include "Subject.h"
class Person;

/*
	Klasse zur Zentralen Datenhaltung im System. 
	Klasse implementiert das Singleton-Pattern.
	Klasse implementiert das Subjekt/Observer-Pattern.
*/
class PersonList: public Subject
{
public:
	
	static PersonList& instance();
	void insert(Person *p);
	Person* get(int id);
	bool del(int id);
	void clear();
	std::vector<Person*>& getAll();

	~PersonList();

protected:
	PersonList();
	PersonList(const PersonList&); /* verhindert, dass eine weitere Instanz via
									Kopier-Konstruktor erstellt werden kann */
	

private:

	std::vector<Person*> list;
};
