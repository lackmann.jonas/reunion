#include "PersonEditView.h"
#include "Person.h"
#include "Organisator.h"
#include "PersonList.h"

PersonEditView::PersonEditView(Person* p, Organisator* user,  QWidget *parent)
	: QDialog(parent), person(p), user(user)
{
	ui.setupUi(this);
	connect(ui.Done, &QPushButton::clicked, this, &PersonEditView::onFertig);

	//Wenn bestehende Person editiert werden soll
	if (person != nullptr) {
		ui.VNameLineEdit->setText(QString::fromStdString(person->getVName()));
		ui.NNameLineEdit->setText(QString::fromStdString(person->getNName()));
		ui.GebNameLineEdit->setText(QString::fromStdString(person->getGebName()));
		if (person->getGebName() != "") {
			ui.GebNameLineEdit->setReadOnly(true); //Geburtsname darf nur einmal gesetzt werden. Somit Lineedit nur noch readonly
		}
		ui.EmailLineEdit->setText(QString::fromStdString(person->getEMail()));
		ui.AdressEdit->document()->setPlainText(QString::fromStdString(person->getAdresse()));
		ui.TelNRLineEdit->setText(QString::fromStdString(person->getTelNR()));
		ui.KommentarEdit->document()->setPlainText(QString::fromStdString(person->getKommentar()));
		
	}
}

void PersonEditView::onFertig() {

	if (person != nullptr) { //Wenn bestehende Person ver�ndert wird
		std::string newNName = ui.NNameLineEdit->text().toStdString();
		std::string newGebName = ui.GebNameLineEdit->text().toStdString();
		std::string newAdr = ui.AdressEdit->toPlainText().toStdString();
		std::string newtel = ui.TelNRLineEdit->text().toStdString();
		std::string newKomment = ui.KommentarEdit->toPlainText().toStdString();

		user->datenBearbeiten(person, newNName, newGebName, newAdr, newtel, newKomment);
	}
	else {//Wenn neue Person angelegt wird

		//Daten aus der UI auslesen
		std::string vName = ui.VNameLineEdit->text().toStdString();
		std::string nName = ui.NNameLineEdit->text().toStdString();
		std::string gebName = ui.GebNameLineEdit->text().toStdString();
		std::string adr = ui.AdressEdit->toPlainText().toStdString();
		std::string telNr = ui.TelNRLineEdit->text().toStdString();
		std::string komment = ui.KommentarEdit->toPlainText().toStdString();
		std::string email = ui.EmailLineEdit->text().toStdString();

		//Person im System anlegen
		user->personHinzufuegen(new Person(vName, nName, email, adr, telNr, komment));



	}
	
	this->close();

}

PersonEditView::~PersonEditView()
{
}
