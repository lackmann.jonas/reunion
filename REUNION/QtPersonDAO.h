#pragma once

#include "PersonDAO.h"
/*
	Implementeriung der PersonDAO unter verwendung des QT-Framework
*/
class QtPersonDAO : public PersonDAO
{
	

public:
	QtPersonDAO();

	virtual bool insert(Person *p);
	virtual bool update(Person *p);
	virtual bool remove(Person *p);
	virtual bool search(Person *p);
	virtual bool select(std::string name, std::vector<Person*>&ps);


	~QtPersonDAO();
};
