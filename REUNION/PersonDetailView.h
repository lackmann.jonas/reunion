#pragma once

#include <QWidget>
#include "ui_PersonDetailView.h"
#include <string>
class Person;
class Organisator;


/*
	Zeigt alle Daten der Person in einem Widget an.
*/
class PersonDetailView : public QWidget
{
	Q_OBJECT

public:
	PersonDetailView(Person *p, Organisator *user, QWidget *parent = Q_NULLPTR);

	
	~PersonDetailView();
private slots:
	void onAendern();
private:
	Person* person;
	Organisator *user;
	Ui::PersonDetailView ui;
	void setNameLabel(std::string name);
	void setGebNameLabel(std::string GebName);
	void setEmailLabel(std::string email);
	void setTelNrLabel(std::string TelNr);
	void setAdressLabel(std::string adr);
	void setKommentLabel(std::string komm);
	void setHistory();
	void update();
};
