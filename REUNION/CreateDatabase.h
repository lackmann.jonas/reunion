#pragma once
#include <string>
/*
	Liefert statische Methode um Datenbank anzulegen.
*/
class CreateDatabase
{
public:
	CreateDatabase();
	static void datenbankErstellen(std::string filename);
	~CreateDatabase();
};

