#include "Date.h"


Date::Date(std::string T, std::string M, std::string J)
	:Tag(T), Monat(M), Jahr(J)
{
}




Date::~Date()
{
}


/*
	Gibt Datum als String zur�ck.
	Format: TT.MM.JJJJ
*/
std::string Date::toString() {
	return Tag + ":" + Monat + ":" + Jahr;
}

/*
	Statische Methode um akutelles Datum zu liefern.
*/
Date Date::getCurrentDate()
{
	std::time_t t = std::time(0);
	std::tm* now = std::localtime(&t);
	return Date(std::to_string(now->tm_mday), std::to_string(now->tm_mon + 1), std::to_string(now->tm_year + 1900));
}