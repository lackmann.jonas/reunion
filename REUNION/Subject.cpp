#include "Subject.h"



Subject::Subject()
{
}

/*
	H�ngt einen neuen Observer an
*/
void Subject::attach(Observer * obs)
{
	observers.push_back(obs);
}

/*
	Benachrichtigt alle Observer ein update zu machen
*/
void Subject::notify()
{
	for (auto x : observers) {
		x->update();
	}
}


Subject::~Subject()
{
}
