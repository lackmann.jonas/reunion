#include "PersonListView.h"
#include "PersonDetailView.h"
#include "Person.h"

PersonListView::PersonListView(QWidget *parent)
	: QListWidget(parent), Observer()
{
	ui.setupUi(this);
	PersonList::instance().attach(this);
	update();
	
}

/*
	Aktualisiert die Anzeige
*/
void PersonListView::update()
{
	/*
		Update mechanismus
	*/
	this->clear();
	for (auto x : PersonList::instance().getAll()) {
		addPerson(x);

	}

}

/*
	Legt den Benutzer fest. 
*/
void PersonListView::setUser(Organisator * user)
{
	this->user = user;
	update();
}

PersonListView::~PersonListView()
{
}

/*
	F�gt eine neue Person hinzu. 
	Erstellt alle erforderlichen Widgets.
*/
void PersonListView::addPerson(Person * p)
{
	QListWidgetItem *item = new QListWidgetItem(this);

	this->addItem(item);
	
	PersonDetailView *custom = new PersonDetailView(p,user,this);
	
	item->setSizeHint(custom->sizeHint());
	this->setItemWidget(item, custom);

}
