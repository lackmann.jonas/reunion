#pragma once

#include <QWidget>
#include "ui_SystemInitScreen.h"
#include <string>

class Hauptorganisator;

/*
	Fenster f�r den initialen Systemstart
*/
class SystemInitScreen : public QWidget
{
	Q_OBJECT

public:
	SystemInitScreen(QWidget *parent = Q_NULLPTR);
	~SystemInitScreen();

private slots:
	void onDatenbankAnlegen();
	void onLogin();

private:
	
	Hauptorganisator *subject;
	std::string DBFileName; 
	Ui::SystemInitScreen ui;
};
