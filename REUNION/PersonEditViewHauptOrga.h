#pragma once

#include <qdialog.h>
#include "ui_PersonEditViewHauptOrga.h"
class Person;
class Hauptorganisator;
/*
	Fenster das der Hautporganisator angezeigt bekommt um Personen zu editieren
*/
class PersonEditViewHauptOrga : public QDialog
{
	Q_OBJECT

public:
	PersonEditViewHauptOrga(Person *p, Hauptorganisator*user ,QWidget *parent = Q_NULLPTR);
	~PersonEditViewHauptOrga();


private slots:
	void onFertig();

private:
	Person *person;
	Hauptorganisator *user;

	Ui::PersonEditViewHauptOrga ui;
};
