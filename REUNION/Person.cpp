#include "Person.h"
#include "PersonHistory.h"
#include "Date.h"
int genID::id = 0;


/*
	Erstellt eine Person, weist ihr eine ID zu und speichert sie in der DB
*/
Person::Person(std::string VName, std::string NName, std::string email)
	:Vorname(VName), Nachname(NName), e_mail(email)
{
	id = genID::generateID();
	DAO = new QtPersonDAO();
	DAO->insert(this);
}

/*
	Erstellt eine Person, weist ihr eine ID zu und speichert sie in der DB
*/
Person::Person(std::string VName, std::string NName, std::string email, std::string adr, std::string telNr, std::string komment)
	:Vorname(VName), Nachname(NName), e_mail(email), Adresse(adr), Tel_Nr(telNr), kommentar(komment)
{
	id = genID::generateID();
	DAO = new QtPersonDAO();
	DAO->insert(this);
}


Person::~Person()
{
}


void Person::aendereNachname(std::string newName)
{
	/*
	code f�r history 	
	*/
	Date currentDate = Date::getCurrentDate();
	history.push_back(new PersonHistory(currentDate, this->Vorname, this->Nachname, this->Geburtsname, this->Adresse, this->e_mail, this->Tel_Nr, this->kommentar));

	this->Nachname = newName;


}


void Person::aendereAdresse(std::string neueAdr)
{
	/*
	code f�r history
	*/
	Date currentDate = Date::getCurrentDate();
	history.push_back(new PersonHistory(currentDate, this->Vorname, this->Nachname, this->Geburtsname, this->Adresse, this->e_mail, this->Tel_Nr, this->kommentar));

	this->Adresse = neueAdr;
}


void Person::aendereTel(std::string neueTel)
{
	/*
	code f�r history
	*/
	Date currentDate = Date::getCurrentDate();
	history.push_back(new PersonHistory(currentDate, this->Vorname, this->Nachname, this->Geburtsname, this->Adresse, this->e_mail, this->Tel_Nr, this->kommentar));

	this->Tel_Nr = neueTel;
}


void Person::aendereKommentar(std::string neuerKommentar)
{
	/*
	code f�r history
	*/
	Date currentDate = Date::getCurrentDate();
	history.push_back(new PersonHistory(currentDate, this->Vorname, this->Nachname, this->Geburtsname, this->Adresse, this->e_mail, this->Tel_Nr, this->kommentar));

	this->kommentar = neuerKommentar;
}


/*
	Falls Geburtsname nachtr�glich hinzugef�gt werden soll. 
	F�hrt �nderung nur aus wenn noch ein Geburtsname gesetzt war.
*/
void Person::setGebName(std::string gebName)
{
	if (this->Geburtsname == "") {
		this->Geburtsname = gebName;
	}
}

/*
	Liefert die komplette Historie der Person
*/
std::vector<PersonHistory*>& Person::getHistory()
{
	return history;
}

std::string Person::getVName()
{
	return this->Vorname;
}

std::string Person::getNName()
{
	return this->Nachname;
}

std::string Person::getGebName()
{
	return this->Geburtsname;
}

std::string Person::getAdresse()
{
	return Adresse;
}

std::string Person::getTelNR()
{
	return Tel_Nr;
}

std::string Person::getEMail()
{
	return e_mail;
}

std::string Person::getKommentar()
{
	return kommentar;
}

