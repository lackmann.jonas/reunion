#include "PersonDetailView.h"
#include "PersonEditView.h"
#include "Person.h"
#include "UserType.h"
#include "Hauptorganisator.h"
#include "PersonEditViewHauptOrga.h"

PersonDetailView::PersonDetailView(Person* p,Organisator *user,QWidget *parent)
	: QWidget(parent), person(p), user(user)
{
	ui.setupUi(this);

	connect(ui.ChangepushButton, &QPushButton::clicked, this,&PersonDetailView::onAendern);

	update();



}

/*
	Aktualisiert alle angezeigten Daten
*/
void PersonDetailView::update() {

	this->setNameLabel(person->getVName() + " " + person->getNName());
	this->setGebNameLabel(person->getGebName());
	this->setEmailLabel(person->getEMail());
	this->setTelNrLabel(person->getTelNR());
	this->setAdressLabel(person->getAdresse());
	this->setKommentLabel(person->getKommentar());
	this->setHistory();
}

/*
	Ruft das Eingabefenster f�r �nderungen an der Person auf.
*/
void PersonDetailView::onAendern() {


	if (UserType::getUserType(user->getEMail()) == UserType::Hauptorganisator) {//Falls user Hauptorganisator ist
		PersonEditViewHauptOrga p(person, static_cast<Hauptorganisator*>(user));
		p.exec();
	}
	else {
		PersonEditView p(person, user);
		p.exec();
	}

	update();
}


void PersonDetailView::setNameLabel(std::string name)
{
	ui.Namelabel->setText(QString::fromStdString(name));
}

void PersonDetailView::setGebNameLabel(std::string GebName)
{
	ui.GebNamelabel->setText(QString::fromStdString(GebName));
}

void PersonDetailView::setEmailLabel(std::string email)
{
	ui.EMailLabel->setText(QString::fromStdString(email));
}

void PersonDetailView::setTelNrLabel(std::string TelNr)
{
	ui.TelNrLabel->setText(QString::fromStdString(TelNr));
}

void PersonDetailView::setAdressLabel(std::string adr)
{
	ui.OrtLabel->setText(QString::fromStdString(adr));
}

void PersonDetailView::setKommentLabel(std::string komm)
{
	ui.KommentarLabel->setText(QString::fromStdString(komm));
}


void PersonDetailView::setHistory()
{
	QString text;
	for (auto x : person->getHistory()) { //iteriert �ber alle History Eintr�ge und Erstellt einen String welcher angezeigt wird.
		text += QString::fromStdString(x->toString());
	}
	ui.HistorieText->setText(text);

}

PersonDetailView::~PersonDetailView()
{
}
