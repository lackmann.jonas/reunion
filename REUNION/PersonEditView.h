#pragma once

#include <QDialog>
#include "ui_PersonEditView.h"

class Person;
class Organisator;

/*
	Klasse f�r das Bearbeitungsfenster f�r Personen.
*/
class PersonEditView : public QDialog
{
	Q_OBJECT

public:
	PersonEditView(Person* p, Organisator* user, QWidget *parent = Q_NULLPTR);

	~PersonEditView();

private slots:
	void onFertig();
	
private:
	Person *person;
	Organisator *user;

	Ui::PersonEditView ui;
};
