#include "REUNION.h"
#include "LoginScreen.h"

REUNION::REUNION(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

}
/*
	Speichert den Loginscreen um ihn ggf. wieder zu �ffenen, 
	und gibt ihn an das Seitenmen� weiter.
*/
void REUNION::setLoginScreen(LoginScreen * s)
{
	this->login = s;
	ui.menu->setLoginScreen(login);
}

/*
	Speichert den eingelogten Nutzer und reicht ihn entsprechend weiter.
*/
void REUNION::setUser(Organisator * user)
{
	this->user = user;
	ui.menu->setUser(user);
	ui.tabelle->setUser(user);
}
